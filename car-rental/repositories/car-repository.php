<?php
class CarRepository
{
    public function __construct($db)
    {
        $this->db = $db;
        $this->table = "car";
    }

    public function add($car)
    {
        $id = $this->db->insert($this->table, ["license_plate", "brand", "chassis", "model", "color"],
        [$car->license_plate, $car->brand, $car->chassis, $car->model, $car->color]);
        return $id;
    }

    public function get_all()
    {
        $result = $this->db->select($this->table, [], []);
        return $result;
    }

    public function get_one($id)
    {
        $result = $this->db->select($this->table, ["id"], [$id]);
        return $result;
    }

    public function remove($id)
    {
        $result = $this->db->delete($this->table, "id", $id);
        return $result;
    }
}