<?php
class RequestedRepository
{
    public function __construct($db)
    {
        $this->db = $db;
        $this->table = "requested";
    }

    public function add($requested)
    {
        $id = $this->db->insert($this->table, ["client_id", "car_id", "date", "amount", "prepaid"],
        [$requested->client_id, $requested->car_id, $requested->date, $requested->amount, $requested->prepaid]);
        return $id;
    }

    public function get_all()
    {
        $result = $this->db->select($this->table, [], []);
        return $result;
    }

    public function get_one($id)
    {
        $result = $this->db->select($this->table, ["id"], [$id]);
        return $result;
    }

    public function remove($id)
    {
        $result = $this->db->delete($this->table, "id", $id);
        return $result;
    }
}