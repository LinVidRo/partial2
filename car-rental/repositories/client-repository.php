<?php
class ClientRepository
{
    public function __construct($db)
    {
        $this->db = $db;
        $this->table = "client";
    }

    public function add($client)
    {
        $id = $this->db->insert($this->table, ["name", "last_name", "cellphone", "ci", "email", "address"],
        [$client->name, $client->last_name, $client->cellphone, $client->ci, $client->email, $client->address]);
        return $id;
    }

    public function get_all()
    {
        $result = $this->db->select($this->table, [], []);
        return $result;
    }

    public function get_one($id)
    {
        $result = $this->db->select($this->table, ["id"], [$id]);
        return $result;
    }

    public function remove($id)
    {
        $result = $this->db->delete($this->table, "id", $id);
        return $result;
    }
}