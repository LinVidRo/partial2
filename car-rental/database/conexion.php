<?php
class DataBase
{
    public function __construct($servername, $username, $password, $database)
    {
        $this->servername = $servername;
        $this->username = $username;
        $this->password = $password;
        $this->database = $database;
        $this->conn = null;
    }
 
    public function connect()
    {
        if ($this->conn === null) {
            $conn = mysqli_connect($this->servername, $this->username, $this->password, $this->database);
            if (!$conn) {
                die("Connection failed: " . mysqli_connect_error());
            } else {
                echo "Connected successfully";
                $this->conn = $conn;
            }
        }   
    }
 
    public function disconnect()
    {
        if ($this->conn != null) {
            mysqli_close($this->conn);
        }
    }

    public function select($table, $columns, $values)
    {
        $query = "SELECT * FROM ".$table;
        if (count($columns) > 0 && count($values) > 0) {
            $query.=" WHERE `".$columns[0]."`=".$values[0]."";
            for ($i=1; $i < count($columns); $i++) { 
                $query.=" AND `".$columns[$i]."`=".$values[$i];
            }
            $query.=";";
        }
        #echo $query;
        $result = mysqli_query( $this->conn, $query ) or die ( "Algo ha ido mal en la consulta a la base de datos");
        #echo $result;
        return $result;
    }

    public function insert($table, $columns, $values)
    {
        $query = "INSERT INTO `".$table."`";
        $query.=" (`".$columns[0]."`";
        for ($i=1; $i < count($columns); $i++) { 
            $query.=", `".$columns[$i]."`";
        }
        $query.=") VALUES";
        $query.=" ('".$values[0]."'";
        for ($i=1; $i < count($values); $i++) { 
            $query.=", '".$values[$i]."'";
        }
        $query.=");";
        #echo $query;
        $result = mysqli_query( $this->conn, $query ) or die ( "Algo ha ido mal en la consulta a la base de datos");
        #echo $result;
        return $result;
    }

    public function update()
    {
    }

    public function delete($table, $column, $value)
    {
        $query = "DELETE * FROM ".$table." WHERE ".$column."=".$value.";";
        $result = mysqli_query( $this->conn, $query ) or die ( "Algo ha ido mal en la consulta a la base de datos");
        return $result;
    }
}
/*$database = new DataBase("localhost", "root", "", "car_rental");
$database->connect();
#$database->insert("client", ["name", "last_name", "cellphone", "ci", "email", "address"], ["Lin", "Vidal", 76453516, 832983, "liviro@gmail.com", "ya tu sabes"]);
$result = $database->select("client", ["id", "ci"], [1, 832983]);
#$result = $database->get("client", [], []);
$database->disconnect();
var_dump($result);

while ($columna = mysqli_fetch_array( $result ))
{
 echo "<tr>";
 echo "<td>" . $columna['name'] . "</td><td>" . $columna['address'] . "</td>";
 echo "</tr>";
}
*/
