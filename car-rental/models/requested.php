<?php
class Requested
{
    public function __construct($id, $client_id, $car_id, $date, $amount, $prepaid)
    {
        $this->id = $id;
        $this->client_id = $client_id;
        $this->car_id = $car_id;
        $this->date = $date;
        $this->amount = $amount;
        $this->prepaid = $prepaid;
    }
}