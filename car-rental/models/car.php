<?php
class Car
{
    public function __construct($id, $license_plate, $brand, $chassis, $model, $color)
    {
        $this->id = $id;
        $this->license_plate = $license_plate;
        $this->brand = $brand;
        $this->chassis = $chassis;
        $this->model = $model;
        $this->color = $color;
    }
}