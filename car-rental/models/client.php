<?php
class Client
{
    public function __construct($id, $name, $last_name, $cellphone, $ci, $email, $address)
    {
        $this->id = $id;
        $this->name = $name;
        $this->cellphone = $cellphone;
        $this->last_name = $last_name;
        $this->ci = $ci;
        $this->email = $email;
        $this->address = $address;
    }
}