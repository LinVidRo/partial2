<?php
include("../database/conexion.php");
include("../models/client.php");
include("../repositories/client-repository.php");
$database = new DataBase("localhost", "root", "", "car_rental");
$client_repository = new ClientRepository($database);
$database->connect();
$method = utf8_decode($_POST['method']);
switch ($method) {
    case 'register':
        $name = utf8_decode($_POST['name']);
        $last_name = utf8_decode($_POST['last_name']);
        $cellphone = utf8_decode($_POST['cellphone']);
        $ci = utf8_decode($_POST['ci']);
        $email = utf8_decode($_POST['email']);
        $address = utf8_decode($_POST['address']);
        $client = new Client(0, $name, $last_name, $cellphone, $ci, $email, $address);
        try {
            $id = $client_repository->add($client);
            echo "<script>alert('Registrado');</script>";
            header('Location: ../index.php');
        } catch (\Throwable $th) {
            //throw $th;
            echo "<script>alert('No Registrado');</script>";
            header('Location: ../views/client-register.html');
        }
        break;
    
    default:
        # code...
        break;
}
$database->disconnect();
