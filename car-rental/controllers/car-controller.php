<?php
include("../database/conexion.php");
include("../models/car.php");
include("../repositories/car-repository.php");
$database = new DataBase("localhost", "root", "", "car_rental");
$car_repository = new CarRepository($database);
$database->connect();
$method = utf8_decode($_POST['method']);
switch ($method) {
    case 'register':
        $license_plate = utf8_decode($_POST['license_plate']);
        $brand = utf8_decode($_POST['brand']);
        $chassis = utf8_decode($_POST['chassis']);
        $model = utf8_decode($_POST['model']);
        $color = utf8_decode($_POST['color']);
        $car = new Car(0, $license_plate, $brand, $chassis, $model, $color);
        try {
            $id = $car_repository->add($car);
            echo "<script>alert('Registrado');</script>";
            header('Location: ../index.php');
        } catch (\Throwable $th) {
            //throw $th;
            echo "<script>alert('No Registrado');</script>";
            header('Location: ../views/car-register.html');
        }
        break;
    
    default:
        # code...
        break;
}
$database->disconnect();
