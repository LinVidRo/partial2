<?php
include("../database/conexion.php");
include("../models/requested.php");
include("../repositories/requested-repository.php");
$database = new DataBase("localhost", "root", "", "car_rental");
$requested_repository = new RequestedRepository($database);
$database->connect();
$method = utf8_decode($_POST['method']);
switch ($method) {
    case 'register':
        $client_id = utf8_decode($_POST['client_id']);
        $car_id = utf8_decode($_POST['car_id']);
        $date = utf8_decode($_POST['date']);
        $amount = utf8_decode($_POST['amount']);
        $prepaid = utf8_decode($_POST['prepaid']);
        $requested = new Requested(0, $client_id, $car_id, $date, $amount, $prepaid);
        try {
            $id = $requested_repository->add($requested);
            echo "<script>alert('Registrado');</script>";
            header('Location: ../index.php');
        } catch (\Throwable $th) {
            //throw $th;
            echo "<script>alert('No Registrado');</script>";
            header('Location: ../views/requested-register.html');
        }
        break;
    
    default:
        # code...
        break;
}
$database->disconnect();
